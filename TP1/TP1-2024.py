# AED - TP1 2024 - Solución sugerida.
# Esta solución sugerida contempla solo elementos contenidos
# entre las Fichas 01 y 05.
# La solución se basa en intentar deducir de qué país es un CP
# en base a condiciones simples por cada país posible, en las que
# se pregunta en forma directa por cada una de las posiciones.
# Otras soluciones basadas en técnicas más avanzadas, son obviamente
# también válidas pero hemos preferido mantener el modelo dentro de
# técnicas elementales, para ajustarnos estrictamente a los contenidos
# de las cinco primeras fichas.

# Títulos y carga de datos.
# print("Correo Argentino - Gestión de envíos")
cp = input("Ingrese el código postal del lugar de destino: ")
direccion = input("Dirección del lugar de destino: ")
tipo = int(input("Tipo de envío (id entre 0 y 6 - ver tabla 2 en el enunciado): "))
pago = int(input("Forma de pago (1: efectivo - 2: tarjeta): "))

# Procesos.
# 1. Determinación del país de destino del envío.
n = len(cp)
if n < 4 or n > 9:
    destino = 'Otro'

else:
    # ¿es Argentina?
    if n == 8:
        if 'A' <= cp[0] <= 'Z' and cp[0] != 'I' and cp[0] != 'O':
            if '0' <= cp[1] <= '9' and '0' <= cp[2] <= '9' and '0' <= cp[3] <= '9' and '0' <= cp[4] <= '9':
                if 'A' <= cp[5] <= 'Z' and 'A' <= cp[6] <= 'Z' and 'A' <= cp[7] <= 'Z':
                    destino = 'Argentina'
                else:
                    destino = 'Otro'
            else:
                destino = 'Otro'
        else:
            destino = 'Otro'

    else:
        # ¿es Bolivia?
        if n == 4:
            if '0' <= cp[0] <= '9' and '0' <= cp[1] <= '9' and '0' <= cp[2] <= '9' and '0' <= cp[3] <= '9':
                destino = 'Bolivia'
            else:
                destino = 'Otro'

        else:
            # ¿es Brasil?
            if n == 9:
                if '0' <= cp[0] <= '9' and '0' <= cp[1] <= '9' and '0' <= cp[2] <= '9' and '0' <= cp[3] <= '9':
                    if '0' <= cp[4] <= '9' and cp[5] == '-':
                        if '0' <= cp[6] <= '9' and '0' <= cp[7] <= '9' and '0' <= cp[8] <= '9':
                            destino = 'Brasil'
                        else:
                            destino = 'Otro'
                    else:
                        destino = 'Otro'
                else:
                    destino = 'Otro'

            else:
                # ¿es Chile?
                if n == 7:
                    if '0' <= cp[0] <= '9' and '0' <= cp[1] <= '9' and '0' <= cp[2] <= '9' and '0' <= cp[3] <= '9':
                        if '0' <= cp[4] <= '9' and '0' <= cp[5] <= '9' and '0' <= cp[6] <= '9':
                            destino = 'Chile'
                        else:
                            destino = 'Otro'
                    else:
                        destino = 'Otro'
                else:
                    # ¿es Paraguay?
                    if n == 6:
                        if '0' <= cp[0] <= '9' and '0' <= cp[1] <= '9' and '0' <= cp[2] <= '9' and '0' <= cp[3] <= '9':
                            if '0' <= cp[4] <= '9' and '0' <= cp[5] <= '9':
                                destino = 'Paraguay'
                            else:
                                destino = 'Otro'
                        else:
                            destino = 'Otro'

                    else:
                        # ¿es Uruguay?
                        if n == 5:
                            if '0' <= cp[0] <= '9' and '0' <= cp[1] <= '9' and '0' <= cp[2] <= '9':
                                if '0' <= cp[3] <= '9' and '0' <= cp[4] <= '9':
                                    destino = 'Uruguay'
                                else:
                                    destino = 'Otro'
                            else:
                                destino = 'Otro'
                        else:
                            destino = 'Otro'

# 2. Determinación de la provincia del lugar de destino.
if destino == 'Argentina':
    p = cp[0]
    if p == 'A':
        provincia = 'Salta'
    elif p == 'B':
        provincia = 'Buenos Aires'
    elif p == 'C':
        provincia = 'Ciudad Autónoma de Buenos Aires'
    elif p == 'D':
        provincia = 'San Luis'
    elif p == 'E':
        provincia = 'Entre Ríos'
    elif p == 'F':
        provincia = 'La Rioja'
    elif p == 'G':
        provincia = 'Santiago del Estero'
    elif p == 'H':
        provincia = 'Chaco'
    elif p == 'J':
        provincia = 'San Juan'
    elif p == 'K':
        provincia = 'Catamarca'
    elif p == 'L':
        provincia = 'La Pampa'
    elif p == 'M':
        provincia = 'Mendoza'
    elif p == 'N':
        provincia = 'Misiones'
    elif p == 'P':
        provincia = 'Formosa'
    elif p == 'Q':
        provincia = 'Neuquén'
    elif p == 'R':
        provincia = 'Río Negro'
    elif p == 'S':
        provincia = 'Santa Fe'
    elif p == 'T':
        provincia = 'Tucumán'
    elif p == 'U':
        provincia = 'Chubut'
    elif p == 'V':
        provincia = 'Tierra del Fuego'
    elif p == 'W':
        provincia = 'Corrientes'
    elif p == 'X':
        provincia = 'Córdoba'
    elif p == 'Y':
        provincia = 'Jujuy'
    elif p == 'Z':
        provincia = 'Santa Cruz'
    else:
        provincia = 'No aplica'

else:
    provincia = "No aplica"


# 3. Determinación del importe inicial a pagar.
importes = (1100, 1800, 2450, 8300, 10900, 14300, 17900)
monto = importes[tipo]
if destino == 'Argentina':
    inicial = monto
else:
    if destino == 'Bolivia' or destino == 'Paraguay' or (destino == 'Uruguay' and cp[0] == '1'):
        inicial = int(monto * 1.20)
    elif destino == 'Chile' or (destino == 'Uruguay' and cp[0] != '1'):
        inicial = int(monto * 1.25)
    elif destino == 'Brasil':
        if cp[0] == '8' or cp[0] == '9':
            inicial = int(monto * 1.20)
        else:
            if cp[0] == '0' or cp[0] == '1' or cp[0] == '2' or cp[0] == '3':
                inicial = int(monto * 1.25)
            else:
                inicial = int(monto * 1.30)
    else:
        inicial = int(monto * 1.50)

# 4. Determinación del valor final del ticket a pagar.
# asumimos que es pago en tarjeta...
final = inicial

# ... y si no lo fuese, la siguiente será cierta y cambiará el valor...
if pago == 1:
    final = int(0.9 * inicial)

# Visualización de resultados.
# print()
# print("TICKET")
print("País de destino del envío:", destino)
print("Provincia destino:", provincia)
print("Importe inicial a pagar:", inicial)
print("Importe final a pagar:", final)
